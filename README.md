# PerpustakaanApps REST API (MongoDB menggunakan FastAPI)
Ini merupakan sebuah aplikasi _**Backend Microservice**_ sederhana untuk dipergunakan sebagai pengembangan aplikasi perpustakaan, terutama nantinya dari sisi Frontend. REST API ini menggabungkan microservice dari dua sisi database yang berbeda, yaitu mongodb sendiri dan juga mysql.
Pada REST API mongodb untuk microservice ini, terdapat beberapa fungsionalitas REST API yang bisa digunakan, seperti **mendapatkan data seluruh buku**, **mendapatkan data buku berdasarkan id buku**, dan terakhir **mendapatkan data buku berdasarkan nama buku**. Microservice mongodb ini akan dihubungkan dengan microservice mysql untuk saling dipadukan antara Customers yang ada pada mysql dengan Books yang ada pada mongodb ini.

List lengkap untuk seluruh endpoint API yang ada pada MongoDBApp ini:

- **[POST]** http://localhost:8000/books
    
    Cara penggunaan: akses melalui _**Postman**_, lalu dilakukan pengaksesan pada url tersebut dengan penggunaan methods _POST_. Hasil akan didapatkan pada **Body** yaitu seluruh data buku. Dengan rincian pada masing-masing data buku tersebut, berisi key dan value yaitu:

    - _id : value berupa ObjectId
    - nama : value berupa string
    - pengarang : value berupa string
    - tahun_terbit : value berupa string
    - genre : value berupa string

- **[POST]** http://localhost:8000/bookbyid

    `Params: id`
    
    Cara penggunaan: akses melalui _**Postman**_, lalu dilakukan pengaksesan pada url tersebut dengan penggunaan methods _POST_. Hasil akan didapatkan pada **Body** yaitu seluruh data buku. Dengan rincian pada masing-masing data buku tersebut, berisi key dan value yaitu:

    - _id : value berupa ObjectId
    - nama : value berupa string
    - pengarang : value berupa string
    - tahun_terbit : value berupa string
    - genre : value berupa string

    Pada endpoint ini, jangan lupa menambahkan **_params_** seperti yang dijelaskan sebelumnya pada Body di Postman. Contoh:

    `{
        "id": "606e84c87e67471003bcbeea"
    }`

- **[POST]** http://localhost:8000/bookbyname

    `Params: nama`
    
    Cara penggunaan: akses melalui _**Postman**_, lalu dilakukan pengaksesan pada url tersebut dengan penggunaan methods _POST_. Hasil akan didapatkan pada **Body** yaitu seluruh data buku. Dengan rincian pada masing-masing data buku tersebut, berisi key dan value yaitu:

    - _id : value berupa ObjectId
    - nama : value berupa string
    - pengarang : value berupa string
    - tahun_terbit : value berupa string
    - genre : value berupa string

    Pada endpoint ini, jangan lupa menambahkan **_params_** seperti yang dijelaskan sebelumnya pada Body di Postman. Contoh:
    
    `{
        "nama": "Harry"
    }`

**ERD Microservice PerpustakaanApps (MySQL and MongoDB)**

![image](ERD.PNG)


**Note**:

> Untuk penggunaan REST API ini, jangan lupa menginstall dan menyiapkan seluruh library ataupun dependencies yang dibutuhkan, terutama FastAPI python.
Karena API ini dalam tahap testing, maka masih dijalankan pada **_server lokal_**.

Akses dengan mengaktifkan server mongodb pada FastAPI : **uvicorn main:app --reload**
